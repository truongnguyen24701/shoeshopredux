import React, { Component } from 'react'
import { connect } from 'react-redux';
import { ADD_TO_CART } from './redux/constants/shoeShopConstant';


// shoeData
class Item_Shoe extends Component {
    render() {
        let { name, description, image } = this.props.shoeData;
        return (
            <div className='col-3'>
                <div className="card" style={{ width: "100%" }}>
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <p className="card-text">{description.length < 40
                            ? description
                            : description.slice(0, 40) + "..."}</p>
                        <button
                            onClick={() => {
                                this.props.handleOnClick(this.props.shoeData)
                            }}
                            className="btn btn-primary">Add to cart</button>
                    </div>
                </div>

            </div>
        )
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleOnClick: (sp) => {
            dispatch({
                type: ADD_TO_CART,
                payload: sp,
            })

        }
    };
}

export default connect(null, mapDispatchToProps)(Item_Shoe)
