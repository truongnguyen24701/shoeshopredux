import { combineReducers } from "redux"
import { shoeShopReducer } from "./shoeShopReducer"


export let rootReducer_shoeShop = combineReducers({
    shoeShopReducer,
}) 