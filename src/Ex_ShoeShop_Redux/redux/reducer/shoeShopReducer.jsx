import { shoeArr } from "../../Data_ShoeShop";
import { ADD_TO_CART, CONG, TRU, XOA } from "../constants/shoeShopConstant";

let intaialState = {
    shoeArr: shoeArr,
    gioHang: [],
};

export let shoeShopReducer = (state = intaialState, { type, payload }) => {
    switch (type) {
        case ADD_TO_CART: {
            let index = state.gioHang.findIndex((item) => {
                return item.id == payload.id;
            });
            let cloneGioHang = [...state.gioHang,]
            
            if (index == -1) {
                //th 1
                let newSp = { ...payload, soLuong: 1 }
                cloneGioHang.push(newSp)
            } else {
                //th2
                cloneGioHang[index].soLuong++
            }
            state.gioHang = cloneGioHang
            return { ...state }
        }
        case XOA: {
            let index = payload
            const data = [...state.gioHang]
            data.splice(index, 1)

            return {
                shoeArr: shoeArr,
                gioHang: data,
            }
        }
        case CONG: {
            let index = payload
            const data = [...state.gioHang].map((item, key) => {
                if (key == index) {
                    item.soLuong += 1
                    return item
                }
                return item
            })
            return {
                shoeArr: shoeArr,
                gioHang: data,
            }
        }
        case TRU: {
        }
        default:
                let index = payload
                const data = [...state.gioHang].map((item, key) => {
                    if (key == index) {
                        if (item.soLuong <= 1) {
                            alert("Vui lòng click chuột vào nút xoá!")
                            return item
                        }
                        item.soLuong -= 1
                        return item
                    }
                    return item
                })
                return {
                    shoeArr: shoeArr,
                    gioHang: data,
                }
            }
            return state;
    }