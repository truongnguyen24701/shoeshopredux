import React, { Component } from 'react'
import { connect } from 'react-redux'
import Item_Shoe from './Item_Shoe'

class List_ShoeShop extends Component {
    renderShoe = () => {
        return this.props.shoeArr.map((item) => {
            return <Item_Shoe
                handleAddToCard={() => { }}
                shoeData={item}
                key={item.id} />
        })
    }
    render() {
        return (
            <div className='row'>{this.renderShoe()}</div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        shoeArr: state.shoeShopReducer.shoeArr,
    };
}

export default connect(mapStateToProps)(List_ShoeShop)
