import React, { Component } from 'react'
import List_ShoeShop from './List_ShoeShop';
import TableGioHang from './TableGioHang';

export default class ShoeShop extends Component {
    render() {
        return (
            <div className='container py-5'>
                <TableGioHang />
                <List_ShoeShop />
            </div>
        )
    }
}
