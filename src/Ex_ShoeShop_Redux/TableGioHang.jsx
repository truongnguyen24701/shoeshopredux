import React, { Component } from 'react'
import { connect } from 'react-redux'
import { XOA, CONG, TRU } from './redux/constants/shoeShopConstant'

class TableGioHang extends Component {
    renderTable = () => {
        return this.props.cart.map((item, index) => {
            return (
                <tr key={item.id}>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price}</td>
                    <td>
                        <button onClick={() => this.props.handleEditTru(index)} className='btn btn-warning'> - </button>
                        <span className='mx-2'>{item.soLuong}</span>
                        <button onClick={() => this.props.handleEditCong(index)} className='btn btn-success'> + </button>
                    </td>
                    <td>
                        <button
                            onClick={() => {
                                this.props.handleDelete(index)
                            }}
                            className='btn btn-danger' >Xoá</button>
                    </td>
                </tr>
            )
        })
    }
    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTable()}
                    </tbody>
                </table>
            </div>
        )
    }
}

let mapStateToProps = (state) => {
    return {
        cart: state.shoeShopReducer.gioHang,
    }
}

let mapDispatchToProps = (dispatch) => {
    return {
        handleDelete: (index) => {
            dispatch({
                type: XOA,
                payload: index,
            })
        },
        handleEditCong: (index) => {
            dispatch({
                type: CONG,
                payload: index,
            })
        },
        handleEditTru: (index) => {
            dispatch({
                type: TRU,
                payload: index,
            })
        },

    };
}



export default connect(mapStateToProps, mapDispatchToProps,)(TableGioHang)
