import logo from './logo.svg';
import './App.css';
import ShoeShop_Redux from "./Ex_ShoeShop_Redux/ShoeShop_Redux"
function App() {
  return (
    <div className="App">
      <ShoeShop_Redux />
    </div>
  );
}

export default App;
